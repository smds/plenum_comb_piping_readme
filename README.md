The plenum to combustor piping assembly connects the air plenum to the combustor. It consists of three pipe segments and five pipe fittings.

This sub-assembly is self-contained and does not require specifying any physical parameters.